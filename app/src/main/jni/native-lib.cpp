#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>

#include <string>

#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

#define  LOG_TAG    "native-lib"
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

/**
 * 手动获取灰度图
 * @param img
 * @param imgDst
 * @param width
 * @param height
 */
void getGrayImg(Mat img,Mat imgDst,jint width,jint height);

/**
 * 初始化bitmap
 * @param env
 * @param srcBitmap
 * @param outBitmap
 * @param srcBitmapPixels
 * @param outBitmapPixels
 * @param srcBitmapInfo
 * @param outBitmapInfo
 * @return
 */
int towBitmapInit(JNIEnv *env, jobject *srcBitmap, jobject *outBitmap, void **srcBitmapPixels, void **outBitmapPixels,
               AndroidBitmapInfo *srcBitmapInfo, AndroidBitmapInfo *outBitmapInfo);

/**
 *
 * @param env
 * @param srcBitmap
 * @param srcBitmapPixels
 * @param srcBitmapInfo
 * @return
 */
int oneBitmapInit(JNIEnv *env, jobject *srcBitmap,void **srcBitmapPixels,AndroidBitmapInfo *srcBitmapInfo);
/**
 * bitmap释放
 * @param env
 * @param srcBitmap
 * @param outBitmap
 */
void towBitmapRelase(JNIEnv *env, jobject *srcBitmap, jobject *outBitmap);

/**
 *
 * @param env
 * @param bitmap
 */
void oneBitmapRelase(JNIEnv *env, jobject *bitmap);
//com_wj_demo_nav_ProgressHelper
extern "C"
JNIEXPORT void JNICALL
Java_com_wj_demo_nav_ProgressHelper_erodeOrDilate(JNIEnv *env, jclass obj,jobject bitmap,jint type) {

    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);

    Mat element = getStructuringElement(MORPH_RECT, Size(15,15));
    if(type==0)
        erode(img,img,element);
    else
        dilate(img,img,element);

    img.release();
    oneBitmapRelase(env,&bitmap);
}

extern "C"
JNIEXPORT void JNICALL
Java_com_wj_demo_nav_ProgressHelper_img_1progress(JNIEnv *env, jclass obj,jobject srcBitmap,jobject outBitmap,jint progress,jint type) {
    const int TYPE_CONTRAST         = 0x1001; //对比度
    const int TYPE_BRIGHT           = 0x1002; //亮度
    const int TYPE_BOX_FILTER       = 0x1003; //方框滤波
    const int TYPE_MEAN_BLUR        = 0x1004; //均值滤波
    const int TYPE_GAUSSIAN_BLUR    = 0x1005; //高斯滤波
    const int TYPE_MEDIAN_BLUR      = 0x1006; //中值滤波
    const int TYPE_BILATERAL_FILTER = 0x1007; //双边滤波



    AndroidBitmapInfo srcBitmapInfo,outBitmapInfo;
    void* srcBitmapPixels,* outBitmapPixels;
    int srcWidth,srcHeight,outWidth,outHeight;

    int res = towBitmapInit(env,&srcBitmap,&outBitmap,&srcBitmapPixels,&outBitmapPixels,&srcBitmapInfo,&outBitmapInfo);
    if(res==-1)
        return;

    srcHeight = srcBitmapInfo.height;
    srcWidth = srcBitmapInfo.width;
    outHeight = srcBitmapInfo.height;
    outWidth = srcBitmapInfo.width;

    Mat srcImg(srcHeight,srcWidth,CV_8UC4,srcBitmapPixels);
    Mat outImg(outHeight,outWidth,CV_8UC4,outBitmapPixels);

    switch(type){
        case TYPE_CONTRAST:
            for(int i=0;i<srcHeight;++i)
                for(int j=0;j<srcWidth;++j)
                    for(int z=0;z<4;++z) //android中共有4层通道需要处理
                        outImg.at<Vec4b>(i,j)[z]=saturate_cast<uchar>((progress*0.01)*(srcImg.at<Vec4b>(i,j)[z]));
            break;
        case TYPE_BRIGHT:
            for(int i=0;i<srcHeight;++i)
                for(int j=0;j<srcWidth;++j)
                    for(int z=0;z<4;++z) //android中共有4层通道需要处理
                        outImg.at<Vec4b>(i,j)[z]=saturate_cast<uchar>((100*0.01)*(srcImg.at<Vec4b>(i,j)[z])+progress);
            break;
        case TYPE_BOX_FILTER:
            boxFilter(srcImg,outImg,-1,Size(progress,progress));
            break;
        case TYPE_MEAN_BLUR:
            blur(srcImg,outImg,Size(progress,progress));
            break;
        case TYPE_GAUSSIAN_BLUR:
            GaussianBlur(srcImg,outImg,Size(progress*2+1,progress*2+1),0,0);
            break;
        case TYPE_MEDIAN_BLUR:
            medianBlur(srcImg,outImg,progress*2+1);
            break;
        case TYPE_BILATERAL_FILTER: //双边滤波异常，需要用像素来实现
            //bilateralFilter(srcImg,outImg,progress,progress*2,progress/2);
            //只支持三通道数据，并且滤波后，是在新的数据空间，因此不能用AndroidBitmapInfo,需要单独实现
            break;
    }

    srcImg.release();
    outImg.release();
    towBitmapRelase(env,&srcBitmap,&outBitmap);
}

extern "C"
JNIEXPORT jintArray JNICALL
Java_com_wj_demo_nav_ProgressHelper_bilateral_1filter(JNIEnv *env, jclass obj,jobject srcBitmap,jint progress){
    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&srcBitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return NULL;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);
    Mat mat;
    cvtColor(img,mat,COLOR_BGRA2RGB);
    Mat out;
    bilateralFilter(mat,out,progress,progress*2,progress/2);
    cvtColor(out,out,COLOR_RGB2RGBA);
    int size = width*height;
    jint *ptr = out.ptr<jint>(0);
    jintArray  result = env->NewIntArray(size);
    env->SetIntArrayRegion(result,0,size,ptr);

    out.release();
    img.release();
    mat.release();
    return result;
}

extern "C"
JNIEXPORT void JNICALL
Java_com_wj_demo_nav_ProgressHelper_gray(JNIEnv *env,  jclass obj,jobject bitmap) {
    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);

//    方式1：此方式会导致outpixels引用发生改变,新生成的img和原img的像素空间地址不一样
    //cvtColor(img,img,COLOR_BGR2GRAY);
    //cvtColor(img,img,COLOR_GRAY2BGRA);

    //方式2：此种计算效率低
    getGrayImg(img,img,width,height);
    /*uchar* ptr = img.ptr(0);
    uchar* outPtr = imgDst.ptr(0);
    for(int i = 0; i < width*height; i ++){
        //计算公式：Y(亮度) = 0.299*R + 0.587*G + 0.114*B
        //对于一个int四字节，其彩色值存储方式为：BGRA
        uchar grayScale = (uchar)(ptr[4*i+2]*0.299 + ptr[4*i+1]*0.587 + ptr[4*i+0]*0.114);
        outPtr[4*i+1] = grayScale;
        outPtr[4*i+2] = grayScale;
        outPtr[4*i+0] = grayScale;
    }*/

    img.release();
    oneBitmapRelase(env,&bitmap);
}

/**
 * ROI兴趣点
 */
extern "C"
JNIEXPORT void JNICALL
Java_com_wj_demo_nav_ProgressHelper_mask(JNIEnv *env, jclass obj,jobject bitmap) {

    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);

    circle(img,Point(width/2,height/2),200,Scalar(0,0,0,0),-1);

    img.release();
    oneBitmapRelase(env,&bitmap);
}

/**
 * 离散傅立叶
 * 需要参考：https://www.cnblogs.com/sdlypyzq/p/3695308.html 通道相关
 * http://www.opencv.org.cn/forum.php?mod=viewthread&tid=4365 转换问题
 */
extern "C"
JNIEXPORT jintArray JNICALL
Java_com_wj_demo_nav_ProgressHelper_dft(JNIEnv *env, jclass obj,jobject bitmap) {
    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return NULL;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);
    //Mat imgGray(width, height,CV_8UC4,ctmpDst);
    LOGE("原始图通道数：%d %d",img.channels(),img.type());
    cvtColor(img,img,COLOR_BGR2GRAY);

    //将输入图像扩展到最佳尺寸，边界用0填充
    int m = getOptimalDFTSize(img.rows);
    int n = getOptimalDFTSize(img.cols);
    Mat padded;
    copyMakeBorder(img,padded,0,m-img.rows,0,n-img.cols,BORDER_CONSTANT,Scalar::all(0));

    //为傅立叶变换的结果（实部和虚部）分配存储空间
    //将planes数组组合合并成一个多通道的数组complexI
    Mat planes[] = {Mat_<float>(padded),Mat::zeros(padded.size(),CV_32F)};
    Mat complexI;
    merge(planes,2,complexI);

    //就地离散傅立叶变换
    dft(complexI,complexI);

   //将复数转换为幅值，即=>log(1+sqrt(Re(DFT(I))^2+Im(DFT(I))^2))
    split(complexI,planes);//将多通道数组complexI分离成几个单通道数组，planes[0]=Re(DFT[I],planes[1]=Im(DFT(I)))
    magnitude(planes[0],planes[1],planes[0]);//planes[0]=magnitude
    Mat magnitudeImage = planes[0];

    //进行对数尺度（logarithmic scale）缩放
    magnitudeImage += Scalar::all(1);
    log(magnitudeImage,magnitudeImage);//求自然对数

    //剪切和重分布幅度图像限
    //若有奇数行或奇数列，进行频谱裁剪,因为图像只能偶数
    magnitudeImage = magnitudeImage(Rect(0,0,magnitudeImage.cols & -4,magnitudeImage.rows & -4));//必须移动四位，保证原始图像和最终图像长宽一致，不能仅仅保持偶数
    //重新排列傅立叶图像中的象限，使得原点位于图像中心
    int cx = magnitudeImage.cols/2;
    int cy = magnitudeImage.rows/2;
    Mat q0(magnitudeImage,Rect(0,0,cx,cy));  //ROI区域在左上
    Mat q1(magnitudeImage,Rect(cx,0,cx,cy));  //ROI区域在右上
    Mat q2(magnitudeImage,Rect(0,cy,cx,cy));  //ROI区域在左下
    Mat q3(magnitudeImage,Rect(cx,cy,cx,cy));  //ROI区域在左下
//交换象限（左上与右下交换）
    Mat tmp;
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);
    //交换象限（右上与左下交换）
    q1.copyTo(tmp);
    q2.copyTo(q1);
    tmp.copyTo(q2);
    //归一化，用0到2之间的浮点值将矩阵变换为可视的图像格式
    normalize(magnitudeImage,magnitudeImage,0,1,NORM_MINMAX);
    convertScaleAbs(magnitudeImage, magnitudeImage,255,0); //将CV_32F转为CV_8U,因为android的Bitmap展示最终还是以RGB形式，CV_32F会被以四通道形式展示(32=4*8)，造成展示结果异常，因此需要先转为CV_8U,
    LOGE("灰度图通道数：%d 大小 %d %d type:%d",magnitudeImage.channels(),magnitudeImage.cols,magnitudeImage.rows,magnitudeImage.type());
    cvtColor(magnitudeImage,magnitudeImage,COLOR_GRAY2BGRA); // CV_8U只满足一通道，无法在android展示，因此需要转为BGRA形式
    int size = width*height;
    jint *ptr = magnitudeImage.ptr<jint>(0);
    jintArray  result = env->NewIntArray(size);
    env->SetIntArrayRegion(result,0,size,ptr);

    magnitudeImage.release();
    img.release();
    oneBitmapRelase(env,&bitmap);
    return result;
}

/**
 * 镜像
 */
extern "C"
JNIEXPORT void JNICALL
Java_com_wj_demo_nav_ProgressHelper_mirror(JNIEnv *env, jclass obj,jobject bitmap) {

    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);
    flip(img,img,1);

    //circle(img,Point(width/2,height/2),200,Scalar(0,0,0,0),-1);

    img.release();
    oneBitmapRelase(env,&bitmap);
}

void getGrayImg(Mat img,Mat imgDst,jint width,jint height){
    uchar* ptr = img.ptr(0);
    uchar* outPtr = imgDst.ptr(0);
    for(int i = 0; i < width*height; i ++){
        //计算公式：Y(亮度) = 0.299*R + 0.587*G + 0.114*B
        //对于一个int四字节，其彩色值存储方式为：BGRA
        uchar grayScale = (uchar)(ptr[4*i+2]*0.299 + ptr[4*i+1]*0.587 + ptr[4*i+0]*0.114);
        outPtr[4*i+1] = grayScale;
        outPtr[4*i+2] = grayScale;
        outPtr[4*i+0] = grayScale;
    }
}

int oneBitmapInit(JNIEnv *env, jobject *srcBitmap,  void **srcBitmapPixels,AndroidBitmapInfo *srcBitmapInfo){
    int srcRet;

    if ((srcRet = AndroidBitmap_getInfo(env, *srcBitmap, srcBitmapInfo)) < 0) {
        LOGE("srcBitmapInfo,AndroidBitmap_getInfo() failed ! error=%d", srcRet);
        return -1;
    }

    if (srcBitmapInfo->format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
        LOGE("srcBitmap format is not RGBA_8888!");
        return -1;
    }

    if ((srcRet = AndroidBitmap_lockPixels(env, *srcBitmap, srcBitmapPixels)) < 0) {
        LOGE("srcBitmap LockPixels Failed return=%d!", srcRet);
        return -1;
    }

    return 0;
}

int towBitmapInit(JNIEnv *env, jobject *srcBitmap, jobject *outBitmap, void **srcBitmapPixels, void **outBitmapPixels,
               AndroidBitmapInfo *srcBitmapInfo, AndroidBitmapInfo *outBitmapInfo){
    int srcRet;
    int outRet;

    if ((srcRet = AndroidBitmap_getInfo(env, *srcBitmap, srcBitmapInfo)) < 0) {
        LOGE("srcBitmapInfo,AndroidBitmap_getInfo() failed ! error=%d", srcRet);
        return -1;
    }

    if (outBitmap!=NULL&&(outRet = AndroidBitmap_getInfo(env, *outBitmap, outBitmapInfo)) < 0) {
        LOGE("outBitmapInfo,AndroidBitmap_getInfo() failed ! error=%d", outRet);
        return -1;
    }

    if (srcBitmapInfo->format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
        LOGE("srcBitmap format is not RGBA_8888!");
        return -1;
    }

    if (outBitmap!=NULL&&outBitmapInfo->format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
        LOGE("outBitmap format is not RGBA_8888!");
        return -1;
    }

    if ((srcRet = AndroidBitmap_lockPixels(env, *srcBitmap, srcBitmapPixels)) < 0) {
        LOGE("srcBitmap LockPixels Failed return=%d!", srcRet);
        return -1;
    }

    if (outBitmap!=NULL&&(outRet = AndroidBitmap_lockPixels(env, *outBitmap, outBitmapPixels)) < 0) {
        LOGE("outBitmap LockPixels Failed return=%d!", outRet);
        return -1;
    }
    return 0;
}

void towBitmapRelase(JNIEnv *env, jobject *srcBitmap, jobject *outBitmap){
    AndroidBitmap_unlockPixels(env, *srcBitmap);
    AndroidBitmap_unlockPixels(env, *outBitmap);
}

void oneBitmapRelase(JNIEnv *env, jobject *bitmap){
    AndroidBitmap_unlockPixels(env, *bitmap);
}