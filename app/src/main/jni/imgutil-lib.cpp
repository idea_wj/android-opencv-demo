#include <jni.h>
#include <android/log.h>
#include <android/bitmap.h>

#include <string>

#include <iostream>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

#define  LOG_TAG    "imgutil"
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)
//AndroidBitmapInfo
void getGrayImg(Mat img,Mat imgDst,jint width,jint height);

/**
 *
 * @param env
 * @param srcBitmap
 * @param srcBitmapPixels
 * @param srcBitmapInfo
 * @return
 */
int oneBitmapInit(JNIEnv *env, jobject *srcBitmap,void **srcBitmapPixels,AndroidBitmapInfo *srcBitmapInfo);

/**
 *
 * @param env
 * @param bitmap
 */
void oneBitmapRelase(JNIEnv *env, jobject *bitmap);

/**
 * ROI 圆
 */
extern "C"
JNIEXPORT void JNICALL
Java_com_wj_library_nav_JNIHelper_mask_1circle(JNIEnv *env, jclass obj,jobject bitmap,jint centX,jint centY,jint radius) {
    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);
    circle(img,Point(centX,centY),radius,Scalar(0,0,0,0),-1);

    img.release();
    oneBitmapRelase(env,&bitmap);
}

/**
 * 镜像
 */
extern "C"
JNIEXPORT void JNICALL
Java_com_wj_library_nav_JNIHelper_mirror(JNIEnv *env, jclass obj,jobject bitmap) {

    AndroidBitmapInfo bitmapInfo;
    void* bitmapPixels;
    int width,height;

    int res = oneBitmapInit(env,&bitmap,&bitmapPixels,&bitmapInfo);
    if(res==-1)
        return;

    height = bitmapInfo.height;
    width = bitmapInfo.width;

    Mat img(height,width,CV_8UC4,bitmapPixels);
    flip(img,img,1);

    //circle(img,Point(width/2,height/2),200,Scalar(0,0,0,0),-1);

    img.release();
    oneBitmapRelase(env,&bitmap);
}

int oneBitmapInit(JNIEnv *env, jobject *srcBitmap,  void **srcBitmapPixels,AndroidBitmapInfo *srcBitmapInfo){
    int srcRet;

    if ((srcRet = AndroidBitmap_getInfo(env, *srcBitmap, srcBitmapInfo)) < 0) {
        LOGE("srcBitmapInfo,AndroidBitmap_getInfo() failed ! error=%d", srcRet);
        return -1;
    }

    if (srcBitmapInfo->format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
        LOGE("srcBitmap format is not RGBA_8888!");
        return -1;
    }

    if ((srcRet = AndroidBitmap_lockPixels(env, *srcBitmap, srcBitmapPixels)) < 0) {
        LOGE("srcBitmap LockPixels Failed return=%d!", srcRet);
        return -1;
    }

    return 0;
}

void oneBitmapRelase(JNIEnv *env, jobject *bitmap){
    AndroidBitmap_unlockPixels(env, *bitmap);
}


