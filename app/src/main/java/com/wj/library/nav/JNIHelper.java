package com.wj.library.nav;

import android.graphics.Bitmap;

import com.wj.library.helper.DialogHelper;

/**
 * Created by idea_wj on 2017/12/25.
 */

public class JNIHelper {

    private static JNIHelper instance=null;

    public static JNIHelper getInstance(){
        if(instance==null){
            synchronized(JNIHelper.class){
                if(null==instance){
                    instance=new JNIHelper();
                }
            }
        }
        return instance;
    }

    /**
     * 在图像上绘制一个圆形透明区域
     *
     * @param bitmap 输入输出图像
     * @param centX  圆心x坐标
     * @param centY  圆心y坐标
     * @param radius 圆半径
     */
    public native void mask_circle(Bitmap bitmap, int centX, int centY, int radius);

    /**
     * 镜像
     * @param //bitmap
     */
    public native void mirror(Bitmap srcBitmap);
}
