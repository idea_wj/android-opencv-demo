package com.wj.demo.domain;

/**
 * Created by idea_wj on 2018/1/20.
 */

public class ImgProgress {
    public static final int TYPE_ERODE            = 0x0001; //腐蚀
    public static final int TYPE_DILATE           = 0x0002; //膨胀
    public static final int TYPE_GRAY             = 0x0003; //灰度
    public static final int TYPE_DFT              = 0x0004; //离散傅立叶
    public static final int TYPE_MASK             = 0x0005; //POI
    public static final int TYPE_MIRROR           = 0x0006; //mirror

    public static final int TYPE_CONTRAST         = 0x1001; //对比度
    public static final int TYPE_BRIGHT           = 0x1002; //亮度
    public static final int TYPE_BOX_FILTER       = 0x1003; //方框滤波
    public static final int TYPE_MEAN_BLUR        = 0x1004; //均值滤波
    public static final int TYPE_GAUSSIAN_BLUR    = 0x1005; //高斯滤波
    public static final int TYPE_MEDIAN_BLUR      = 0x1006; //中值滤波
    public static final int TYPE_BILATERAL_FILTER = 0x1007; //双边滤波

    public static final int ITEM_TYPE_SEEK_BAR    = 0x5001; //recycleview中输入按钮布局
    public static final int ITEM_TYPE_BUTTON      = 0x5002; //属于seekbar布局

    private String name;
    private int    type;
    private int    value;
    private int    max;
    private int    itemType; //属于recyclerview中哪种布局

    public ImgProgress(String name,int value,int max,int type,int itemType){
        this.name = name;
        this.type = type;
        this.value = value;
        this.max = max;
        this.itemType = itemType;
    }

    public ImgProgress(String name,int type,int itemType){
        this.name = name;
        this.type = type;
        this.itemType = itemType;
    }


    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
