package com.wj.demo.nav;

import android.graphics.Bitmap;

import com.wj.library.nav.JNIHelper;

/**
 * Created by idea_wj on 2018/1/20.
 */

//com_wj_demo_nav_ProgressHelper
public class ProgressHelper {
    private static ProgressHelper instance=null;

    public static ProgressHelper getInstance(){
        if(instance==null){
            synchronized(ProgressHelper.class){
                if(null==instance){
                    instance=new ProgressHelper();
                }
            }
        }
        return instance;
    }

    /**
     *
     * @param srcBitmap
     * @param type 0 腐蚀 1 膨胀
     */
    public native void erodeOrDilate(Bitmap srcBitmap, int type);

    /**
     * 灰度图
     * @param bitmap
     */
    public native void gray(Bitmap bitmap);

    /**
     * 图像的常规处理，seekbar
     * @param srcBitmap
     * @param outBitmap
     * @param progress
     * @param type
     */
    public native void img_progress(Bitmap srcBitmap,Bitmap outBitmap,int progress,int type);

    public native int[] bilateral_filter(Bitmap srcBitmap,int progress);

    /**
     * 离散傅立叶变换
     * @param bitmap
     * @return
     */
    public native int[] dft(Bitmap bitmap);

    /**
     * ROI区域
     * @param //bitmap
     */
    public native void mask(Bitmap srcBitmap);

    /**
     * 镜像
     * @param //bitmap
     */
    public native void mirror(Bitmap srcBitmap);

    static{
//        if (!OpenCVLoader.initDebug()){// 默认加载opencv_java.so库
//        }
        System.loadLibrary("native-lib");
        System.loadLibrary("imgutil-lib");
    }
}
