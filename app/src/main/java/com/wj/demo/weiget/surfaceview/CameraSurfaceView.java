package com.wj.demo.weiget.surfaceview;//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class CameraSurfaceView extends SurfaceView implements Callback, PreviewCallback {
    private final String TAG = this.getClass().getSimpleName();
    private Camera                               mCamera;
    private int                                  mWidth;
    private int                                  mHeight;
    private int                                  mFormat;
    private CameraSurfaceView.OnCameraListener   mOnCameraListener;
    private FrameHelper                          mFrameHelper;
    private Timer                              mTimer;
    private long    PERIOD         = 100;   //每隔指定值的毫秒时间后，运行一次帧检测
    private boolean isAllowPreview = true;

    public CameraSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.onCreate();
    }

    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.onCreate();
    }

    public CameraSurfaceView(Context context) {
        super(context);
        this.onCreate();
    }

    private void onCreate() {
        SurfaceHolder arg0 = this.getHolder();
        arg0.setType(3);
        arg0.addCallback(this);
        this.mFrameHelper = new FrameHelper();
    }

    /**
     * 设置帧率
     *
     * @param num
     */
    public void setFTPS(int num) {
        PERIOD = 1000 / num;
    }

    private boolean openCamera() {

        //控制帧率
        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isAllowPreview)
                    isAllowPreview = true;
            }
        }, 0, PERIOD); //立即执行，之后每300毫秒执行一次
        try {

            if (this.mCamera != null) {
                this.mCamera.reconnect();
            } else
            if (this.mOnCameraListener != null) {
                this.mCamera = this.mOnCameraListener.setupCamera();
            }

            if (this.mCamera != null) {
                this.mCamera.setPreviewDisplay(this.getHolder());
                Size e = this.mCamera.getParameters().getPreviewSize();
                this.mWidth = e.width;
                this.mHeight = e.height;
                this.mFormat = this.mCamera.getParameters().getPreviewFormat();
                int lineBytes = e.width * ImageFormat.getBitsPerPixel(this.mFormat) / 8;
                this.mCamera.addCallbackBuffer(new byte[lineBytes * this.mHeight]);
                this.mCamera.addCallbackBuffer(new byte[lineBytes * this.mHeight]);
                this.mCamera.addCallbackBuffer(new byte[lineBytes * this.mHeight]);
                this.mCamera.setPreviewCallbackWithBuffer(this);
                return true;
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }
        return false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (this.mOnCameraListener != null) {
            this.mOnCameraListener.setupChanged(format, width, height);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        /*Canvas canvas = holder.lockCanvas();

        //绘制之前先对画布进行翻转
        canvas.scale(-1,1, getWidth()/2,getHeight()/2);
        holder.unlockCanvasAndPost(canvas);*/
        if (this.openCamera()) {
            //Log.e(this.TAG, "preview size = " + this.mCamera.getParameters().getPreviewSize().width + "," + this.mCamera.getParameters().getPreviewSize().height);
            if (this.mOnCameraListener != null && !this.mOnCameraListener.startPreviewLater()) {
                this.mCamera.startPreview();
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mCamera != null) {
            this.mCamera.setPreviewCallbackWithBuffer(null);
            this.mCamera.setPreviewCallback(null);
            this.mCamera.stopPreview();
            this.mCamera.release();
            mTimer.cancel();
            mTimer = null;
            this.mCamera = null;
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (isAllowPreview) {
            long timestamp = System.nanoTime();
            this.mFrameHelper.printFPS();
            isAllowPreview = false;
            float widthScale = 0f;
            float heightScale = 0f;
            /*float widthScale = mGLSurfaceView.getWidth() / (mWidth * 1.0f);
            float heightScale = mGLSurfaceView.getHeight() / (mHeight * 1.0f);*/

            widthScale = getWidth() / (mWidth * 1.0f);
            heightScale = getHeight() / (mHeight * 1.0f);
            if (this.mOnCameraListener != null) {
                this.mOnCameraListener.onPreview(data, widthScale, heightScale, timestamp);
            }

        }
        if (this.mCamera != null)
            this.mCamera.addCallbackBuffer(data);
    }

    /**
     * 是否打印fps日志
     *
     * @param isPrint
     */
    public void debugPrintFps(boolean isPrint) {
        mFrameHelper.enable(isPrint);
    }

    public void setOnCameraListener(CameraSurfaceView.OnCameraListener l) {
        this.mOnCameraListener = l;
    }

    public interface OnCameraListener {
        Camera setupCamera();

        void setupChanged(int var1, int var2, int var3);

        boolean startPreviewLater();

        void onPreview(byte[] data, float widthScale, float heightScale, long timestamp);
    }
}