package com.wj.demo.weiget.surfaceview;//



import android.util.Log;

/**
 * @author sangular-wj
 */
public class FrameHelper {
    private final String TAG = this.getClass().getSimpleName();
    private long mStartTime;
    private long mFrames;
    private boolean isShow = true;

    public FrameHelper() {
        this.reset();
    }

    public void printFPS() {
        if(this.isShow) {
            if(this.mStartTime == 0L) {
                this.mStartTime = System.currentTimeMillis();
                this.mFrames = 0L;
            } else {
                ++this.mFrames;
                if(this.mFrames >= 10L) {
                    Log.e(this.TAG, "FPS = " + (int)(1000.0D * (double)this.mFrames / (double)(System.currentTimeMillis() - this.mStartTime)));
                    this.mStartTime = 0L;
                }
            }
        }
    }

    public void reset() {
        this.mStartTime = 0L;
        this.mFrames = 0L;
    }

    public void enable(boolean show) {
        if(this.isShow) {
            this.reset();
        }

        this.isShow = show;
    }
}
