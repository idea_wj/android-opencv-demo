package com.wj.demo.app;

import android.content.ClipData;
import android.os.Environment;

import com.wj.demo.domain.Item;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by idea_wj on 2016/6/1.
 * @version 1.0
 * 静态常量
 */
public class Constants extends com.wj.library.common.Constants {
    public static String MAIN_PATH = Environment.getExternalStorageDirectory().getPath();
    public static String IMAGE_CACHE = MAIN_PATH + "/.cache_image";

    public static int IMAGE_WIDTH  = 960;
    public static int IMAGE_HEIGHT = 1280;

}
