package com.wj.demo.helper;

import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.text.format.DateFormat;
import android.util.Log;

import com.wj.demo.app.Constants;
import com.wj.demo.weiget.surfaceview.CameraSurfaceView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sangular-wj on 2017/9/17.
 */

public class CameraSurfaceHelper implements CameraSurfaceView.OnCameraListener {
    public final String TAG = CameraSurfaceHelper.class.getSimpleName();

    private CameraSurfaceView             mCameraSurfaceView;
    private OnCameraListener              mOnCameraListener;
    private OnPreviewFrame                mOnPreviewFrame;
    private OnSetUpCameraListener         mOnSetUpCameraListener;
    private Camera                        mCamera;
    private int                           mOrientation;
    private Comparator<Camera.Size>       mSizeComparator;
    private HashMap<String, Camera.Size>  mSizeCache         = new HashMap<>();

    private boolean isPreview = false;

    private static CameraSurfaceHelper instance=null;

    public static CameraSurfaceHelper getInstance(CameraSurfaceView cameraSurfaceView, int orientation){
        if(instance==null){
            synchronized(CameraSurfaceHelper.class){
                if(null==instance)
                    instance=new CameraSurfaceHelper(cameraSurfaceView,orientation);
            }
        }
        return instance;
    }

    public CameraSurfaceHelper(CameraSurfaceView cameraSurfaceView, int orientation) {
        mCameraSurfaceView = cameraSurfaceView;
        mCameraSurfaceView.setOnCameraListener(this);
        mOrientation = orientation;


        mSizeComparator = new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size lhs, Camera.Size rhs) {
                if (lhs.width == rhs.width) {
                    return 0;
                } else if (lhs.width > rhs.width) {
                    return 1;
                } else {
                    return -1;
                }
            }
        };
    }

    private Camera.PictureCallback jpegCallback = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera) {
            Log.e(TAG, "图像容量：" + bytes.length);
            try {
                File file = new File(Constants.IMAGE_CACHE);
                if (!file.exists())
                    file.mkdir();
                String pictureName = new DateFormat().format("/yyyyMMddHHmmss", new Date()).toString() + ".jpg";
                File f = new File(file.getAbsolutePath() + pictureName);
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bytes);
                fos.close();
                mOnCameraListener.takePictureOver(f.getAbsolutePath());
            } catch (FileNotFoundException e) {
                Log.e(TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.e(TAG, "Error accessing file: " + e.getMessage());
            }
        }
    };

    public void setOnCameraListener(OnCameraListener onCameraListener) {
        mOnCameraListener = onCameraListener;
    }

    public void setOnPreviewListener(OnPreviewFrame onPreviewFrame) {
        mOnPreviewFrame = onPreviewFrame;
    }

    public void setOnSetUpCameraListener(OnSetUpCameraListener onSetUpCameraListener) {
        mOnSetUpCameraListener = onSetUpCameraListener;
    }

    /**
     * 照相
     */
    public void takePicture() {
        mCamera.takePicture(null, null, jpegCallback);
        isPreview = false;
    }

    public void startPreView() {
        if(mCamera!=null&&!isPreview) {
            mCamera.startPreview();
            isPreview = true;
        }
    }

    public void stopPreview(){
        mCamera.stopPreview();
        isPreview = false;
    }

    /**
     * 自动对焦
     */
    public void autoFocus() {
        if (mCamera != null)
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean b, Camera camera) {

                }
            });
    }

    @Override
    public Camera setupCamera() {
        mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);
        mCamera.setDisplayOrientation(mOrientation);
        try {
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPreviewSize(Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT);
            parameters.setPreviewFormat(ImageFormat.NV21);
            parameters.setPictureSize(Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT);
            parameters.setPictureFormat(ImageFormat.NV21);
            List<Camera.Size> s = parameters.getSupportedPreviewSizes();
            /*for(int i=0;i<s.size();++i){
                Log.e(TAG,"width:"+s.get(i).width+"height:"+s.get(i).height);
            }*/
            mCamera.setParameters(parameters);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mCamera != null) {
            Constants.IMAGE_WIDTH = mCamera.getParameters().getPreviewSize().width;
            Constants.IMAGE_HEIGHT = mCamera.getParameters().getPreviewSize().height;
            //Log.e(TAG, Constants.IMAGE_HEIGHT + " " + Constants.IMAGE_WIDTH);
        }

        if (mOnSetUpCameraListener != null) {
            mOnSetUpCameraListener.onSetCameraFinish(mCamera);
        }

        return mCamera;
    }

    public void resetCameraParam(int minWidth, float ratio) {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setDisplayOrientation(mOrientation);
            Camera.Parameters parameters = mCamera.getParameters();
            parameters.setPictureFormat(ImageFormat.JPEG);
            parameters.setPreviewFormat(ImageFormat.NV21);
            parameters.setJpegQuality(100); // 设置照片质量

            Camera.Size previewSize = getPropPreviewSize(mCamera, ratio, minWidth);
            Camera.Size pictureSize = getPropPictureSize(mCamera, ratio, minWidth);

            parameters.setPreviewSize(previewSize.width, previewSize.height);
            parameters.setPictureSize(pictureSize.width, pictureSize.height);

            try {
                mCamera.setParameters(parameters);
                mCamera.startPreview();
            } catch (Exception ignore) {

            }
        }
    }

    @Override
    public void setupChanged(int i, int i1, int i2) {

    }

    @Override
    public boolean startPreviewLater() {
        return false;
    }

    @Override
    public void onPreview(byte[] data, float widthScale, float heightScale, long timestamp) {
            if (mOnPreviewFrame != null)
                mOnPreviewFrame.previewFrame(data,widthScale,heightScale);
    }


    public void setOrientation(int orientation) {
        mOrientation = orientation;
    }

    /**
     * 获取合适预览大小
     *
     * @param th       宽高比,大于1(1,1.33,1.77)
     * @param minWidth 限制宽度不至于过小
     * @return size
     */
    private Camera.Size getPropPreviewSize(Camera cameraInst, float th, int minWidth) {
        Camera.Size result = mSizeCache.get("getPropPreviewSize");
        if (result != null)
            return result;
        List<Camera.Size> list = cameraInst.getParameters().getSupportedPreviewSizes();
        Collections.sort(list, mSizeComparator);
        for (int i = 0; i < list.size(); i++) {
            Camera.Size s = list.get(i);
            if (equalRate(s, th)) {
                if (minWidth > 100000) {
                    if (s.width * s.height >= minWidth) {
                        return s;
                    }
                } else {
                    if (s.width >= minWidth) {
                        return s;
                    }
                }
            }
        }

        for (int i = list.size() - 1; i >= 0; i--) {
            Camera.Size s = list.get(i);
            if (equalRate(s, th)) {
                return s;
            }
        }
        result = list.get(0);
        mSizeCache.put("getPropPreviewSize", result);
        return result;
    }

    /**
     * 获取合适照片大小
     *
     * @param th       宽高比,大于1(1,1.33,1.77)
     * @param minWidth 最小宽度
     * @return size
     */
    private Camera.Size getPropPictureSize(Camera cameraInst, float th, int minWidth) {
        Camera.Size result = mSizeCache.get("getPropPictureSize");
        if (result != null)
            return result;
        List<Camera.Size> list = cameraInst.getParameters().getSupportedPictureSizes();
        Collections.sort(list, mSizeComparator);
        for (int i = 0; i < list.size(); i++) {
            Camera.Size s = list.get(i);
            if (equalRate(s, th)) {
                if (minWidth > 100000) {
                    if (s.width * s.height >= minWidth) {
                        return s;
                    }
                } else {
                    if (s.width >= minWidth) {
                        return s;
                    }
                }
            }
        }

        for (int i = list.size() - 1; i >= 0; i--) {
            Camera.Size s = list.get(i);
            if (equalRate(s, th)) {
                return s;
            }
        }
        result = list.get(0);
        mSizeCache.put("getPropPictureSize", result);
        return result;
    }

    /**
     * 尺寸比率是否相等
     *
     * @param s    size
     * @param rate w/h比值
     * @return bool
     */

    private static boolean equalRate(Camera.Size s, float rate) {
        float r = s.width / (s.height * 1.0f);
        return Math.abs(r - rate) <= 0.02;
    }

    public void release() {
        instance = null;
    }

    public interface OnCameraListener {
        void takePictureOver(String path);
    }

    public interface OnPreviewFrame {
        void previewFrame(byte[] data, float widthScale, float heightScale);
    }

    public interface OnSetUpCameraListener {
        void onSetCameraFinish(Camera camera);
    }
}
