package com.wj.demo.ui.adapter;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.wj.demo.R;
import com.wj.demo.domain.ImgProgress;

import java.util.ArrayList;

/**
 * 适配器
 * Created by wuj on 2016/6/1.
 * @version 1.0
 */
public class AdapterImageProgress extends RecyclerView.Adapter<ImgProgressViewHolder> {

    private AppCompatActivity      mActivity;
    private ArrayList<ImgProgress> mItems;
    private Bitmap                 mSrcBitmap;
    private Bitmap                 mTmpBitmap;
    private ImageView              mIvShow;

    public AdapterImageProgress(AppCompatActivity activity, ArrayList<ImgProgress> items,Bitmap srcBitmap,Bitmap tmpBitmap,ImageView ivShow){
        this.mActivity = activity;
        this.mItems = items;
        this.mSrcBitmap=srcBitmap;
        this.mTmpBitmap=tmpBitmap;
        this.mIvShow = ivShow;
    }


    @Override
    public ImgProgressViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = null;
        switch (viewType){
            case ImgProgress.ITEM_TYPE_BUTTON:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_progress_2, parent, false);
                break;
            case ImgProgress.ITEM_TYPE_SEEK_BAR:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_progress_1, parent, false);
                break;
            default:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_progress_1, parent, false);
        }
        return new ImgProgressViewHolder(v, mActivity,mIvShow,mSrcBitmap,mTmpBitmap,viewType);
    }

    @Override
    public void onBindViewHolder(ImgProgressViewHolder holder, int position) {
        holder.bindItem(mItems.get(position));

    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).getItemType();
    }
}
