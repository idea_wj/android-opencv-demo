package com.wj.demo.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.wj.demo.R;
import com.wj.library.helper.UIHelper;
import com.wj.library.util.SystemUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gqj3375 on 2017/2/24.
 * 权限校验页面
 */

public class PermissionActivity extends BaseActivity {
    public static int PERMISSION_REQ = 0x1234;

    private static final String[] NEED_APPLY_PERMISSION = new String[]{
            Manifest.permission.INTERNET,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private List<String> mRequestPermission = new ArrayList<>();

    @Override
    protected int getLayout() {
        return R.layout.activity_permission;
    }

    @Override
    protected void initViewAndData() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        if (null != permissions
                && null != grantResults
                && SystemUtil.compareVersionCode(Build.VERSION_CODES.M)
                && requestCode == PERMISSION_REQ) {

            outer:
            for (int i = 0; i < grantResults.length; i++) {
                inner:
                for (String one : NEED_APPLY_PERMISSION) {
                    if (permissions[i].equals(one) && grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        mRequestPermission.remove(one);
                        break inner;
                    }
                }
            }
        }

        startActivity();
    }

    public void startActivity() {
        if (mRequestPermission.isEmpty()) {
            UIHelper.getInstance().startActivityAndFinish(mBaseActivity, new Intent(mBaseActivity, MainActivity.class));
        } else {
            Toast.makeText(this, "PERMISSION DENIED!", Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //跳转到权限设置页面
                    Intent intent = SystemUtil.getPermissionSettingIntent(mBaseActivity);
                    UIHelper.getInstance().startActivity(mBaseActivity, intent);
                }
            }, 3000);
        }
    }

    /**
     * 校验权限
     */
    private void checkPermission() {
        if (SystemUtil.compareVersionCode(Build.VERSION_CODES.M)) {
            for (String one : NEED_APPLY_PERMISSION) {
                if (PackageManager.PERMISSION_GRANTED != mBaseActivity.checkPermission(one, Process.myPid(), Process.myUid())) {
                    mRequestPermission.add(one);
                }
            }
            if (!mRequestPermission.isEmpty()) {
                mBaseActivity.requestPermissions(mRequestPermission.toArray(new String[mRequestPermission.size()]), PERMISSION_REQ);
                return;
            }
        }
        startActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPermission();
    }
}
