package com.wj.demo.ui.adapter;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.wj.demo.R;
import com.wj.demo.domain.ImgProgress;
import com.wj.demo.nav.ProgressHelper;
import com.wj.library.listener.MyOnClickListener;

import java.util.ArrayList;

/**
 * 创建列表项
 * Created by wuj on 2016/6/1.
 * @version 1.0
 */
public class ImgProgressViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = ImgProgressViewHolder.class.getName();
    private AppCompatActivity      mActivity;
    private View                   mView;
    private SeekBar                mSbProgress;
    private LinearLayout           mLlItem;
    private TextView               mTvName;
    private TextView               mTvValue;
    private ImageView              mIvShow;
    private Bitmap                 mSrcBitmap;
    private Bitmap                 mTmpBitmap;

    public ImgProgressViewHolder(View itemView, AppCompatActivity activity,ImageView ivShow, Bitmap srcBitmap,  Bitmap tmpBitmap,int viewType) {
        super(itemView);
        this.mView = itemView;
        this.mActivity = activity;
        switch (viewType){
            case ImgProgress.ITEM_TYPE_SEEK_BAR:
                mTvName = (TextView)mView.findViewById(R.id.tv_name);
                mTvValue = (TextView)mView.findViewById(R.id.tv_value);
                mSbProgress = (SeekBar)mView.findViewById(R.id.sb_progress);
                break;
            case ImgProgress.ITEM_TYPE_BUTTON:
                mLlItem = (LinearLayout) mView.findViewById(R.id.ll_item);
                mTvName = (TextView)mView.findViewById(R.id.tv_name);
                break;
        }


        mIvShow = ivShow;
        mSrcBitmap = srcBitmap;
        mTmpBitmap = tmpBitmap;
    }

    /**
     * 将数据绑定到item
     */
    public void bindItem(ImgProgress imgProgress){
        switch (imgProgress.getItemType()){
            case ImgProgress.ITEM_TYPE_SEEK_BAR:
                typeSeekBar(imgProgress);
                break;
            case ImgProgress.ITEM_TYPE_BUTTON:
                typeButton(imgProgress);
                break;
        }
    }

    private void typeSeekBar(final ImgProgress imgProgress){
        mTvName.setText(imgProgress.getName());
        mTvValue.setText(imgProgress.getValue()+"");
        mSbProgress.setMax(imgProgress.getMax());
        //mSbProgress.setProgress(imgProgress.getValue());
        mSbProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                int progress = seekBar.getProgress();
                if(progress==0)
                    progress=1;
                mTvValue.setText(progress+"");
                switch (imgProgress.getType()){
                    case ImgProgress.TYPE_CONTRAST:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,ImgProgress.TYPE_CONTRAST);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_BRIGHT:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,ImgProgress.TYPE_BRIGHT);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_BOX_FILTER:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,imgProgress.TYPE_BOX_FILTER);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_MEAN_BLUR:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,imgProgress.TYPE_MEAN_BLUR);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_GAUSSIAN_BLUR:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,imgProgress.TYPE_GAUSSIAN_BLUR);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_MEDIAN_BLUR:
                        ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,imgProgress.TYPE_MEDIAN_BLUR);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_BILATERAL_FILTER:
                        //不能使用img_progress（）方法，具体原因，在c++层有解释
                        //ProgressHelper.getInstance().img_progress(mSrcBitmap,mTmpBitmap,progress,imgProgress.TYPE_BILATERAL_FILTER);
                        int[] s = ProgressHelper.getInstance().bilateral_filter(mSrcBitmap,progress);
                        int width = mSrcBitmap.getWidth();
                        int height = mSrcBitmap.getHeight();
                        if(s!=null){
                            mTmpBitmap.setPixels(s,0,width,0,0,width,height);
                            mIvShow.setImageBitmap(mTmpBitmap);
                        }
                        break;
                }
            }
        });
    }

    private void  typeButton(final ImgProgress imgProgress){
        mTvName.setText(imgProgress.getName());
        mLlItem.setOnClickListener(new MyOnClickListener(){

            @Override
            public void myOnClick(View view) {
                switch (imgProgress.getType()){
                    case ImgProgress.TYPE_ERODE:
                        ProgressHelper.getInstance().erodeOrDilate(mTmpBitmap,0);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_DILATE:
                        ProgressHelper.getInstance().erodeOrDilate(mTmpBitmap,1);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_GRAY:
                        ProgressHelper.getInstance().gray(mTmpBitmap);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_DFT:
                        int[] s = ProgressHelper.getInstance().dft(mTmpBitmap);
                        int width = mSrcBitmap.getWidth();
                        int height = mSrcBitmap.getHeight();
                        if(s!=null){
                            mTmpBitmap.setPixels(s,0,width,0,0,width,height);
                            mIvShow.setImageBitmap(mTmpBitmap);
                        }
                        break;
                    case ImgProgress.TYPE_MASK:
                        ProgressHelper.getInstance().mask(mTmpBitmap);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                    case ImgProgress.TYPE_MIRROR:
                        ProgressHelper.getInstance().mirror(mTmpBitmap);
                        mIvShow.setImageBitmap(mTmpBitmap);
                        break;
                }
            }
        });
    }
}
