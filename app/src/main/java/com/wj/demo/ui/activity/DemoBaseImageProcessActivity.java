package com.wj.demo.ui.activity;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wj.demo.R;
import com.wj.demo.domain.ImgProgress;
import com.wj.demo.ui.adapter.AdapterImageProgress;
import com.wj.demo.ui.base.BaseActivity;
import com.wj.library.helper.ToolbarHelper;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * 图像的基本处理
 */
public class DemoBaseImageProcessActivity extends BaseActivity {
    private static String TAG = DemoBaseImageProcessActivity.class.getSimpleName();

    @BindView(R.id.iv_src)
    ImageView mIvSrc;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tv_title)
    TextView mTvTitle;

    @BindView(R.id.tv_reset)
    TextView mTvReset;

    @BindView(R.id.rv_img_progress)
    RecyclerView mRvImgProgress;

    private Bitmap mSrcBitmap;
    private Bitmap mTmpBitmap;
    private Canvas mCanvas;
    private Paint  mPaint;
    private int    mSrcBitmapWidth;
    private int    mSrcBitmapHeight;

    private AdapterImageProgress mAdapterImageProgress;

    private ArrayList<ImgProgress> mItems;

    @Override
    protected int getLayout() {
        return R.layout.activity_base_image_process;
    }

    @Override
    protected void initViewAndData() {
        mItems = new ArrayList<>();
        ImgProgress item0001 = new ImgProgress("腐蚀",ImgProgress.TYPE_ERODE,ImgProgress.ITEM_TYPE_BUTTON);
        ImgProgress item0002 = new ImgProgress("膨胀",ImgProgress.TYPE_DILATE,ImgProgress.ITEM_TYPE_BUTTON);
        ImgProgress item0003 = new ImgProgress("灰度",ImgProgress.TYPE_GRAY,ImgProgress.ITEM_TYPE_BUTTON);
        ImgProgress item0004 = new ImgProgress("离散傅立叶变换",ImgProgress.TYPE_DFT,ImgProgress.ITEM_TYPE_BUTTON);
        ImgProgress item0005 = new ImgProgress("MASK区域透明",ImgProgress.TYPE_MASK,ImgProgress.ITEM_TYPE_BUTTON);
        ImgProgress item0006 = new ImgProgress("镜像相反",ImgProgress.TYPE_MIRROR,ImgProgress.ITEM_TYPE_BUTTON);

        ImgProgress item1001 = new ImgProgress("对比度",100,300,ImgProgress.TYPE_CONTRAST,ImgProgress.ITEM_TYPE_SEEK_BAR); //默认需要100
        ImgProgress item1002 = new ImgProgress("亮  度",1,200,ImgProgress.TYPE_BRIGHT,ImgProgress.ITEM_TYPE_SEEK_BAR); //默认需要0
        ImgProgress item1003 = new ImgProgress("方框滤波",1,50,ImgProgress.TYPE_BOX_FILTER,ImgProgress.ITEM_TYPE_SEEK_BAR);
        ImgProgress item1004 = new ImgProgress("均值滤波",1,50,ImgProgress.TYPE_MEAN_BLUR,ImgProgress.ITEM_TYPE_SEEK_BAR);
        ImgProgress item1005 = new ImgProgress("高斯滤波",1,50,ImgProgress.TYPE_GAUSSIAN_BLUR,ImgProgress.ITEM_TYPE_SEEK_BAR);
        ImgProgress item1006 = new ImgProgress("中值滤波",1,50,ImgProgress.TYPE_MEDIAN_BLUR,ImgProgress.ITEM_TYPE_SEEK_BAR);
        ImgProgress item1007 = new ImgProgress("双边滤波",1,100,ImgProgress.TYPE_BILATERAL_FILTER,ImgProgress.ITEM_TYPE_SEEK_BAR);

        mItems.add(item0001);
        mItems.add(item0002);
        mItems.add(item0003);
        mItems.add(item0004);
        mItems.add(item0005);
        mItems.add(item0006);

        mItems.add(item1001);
        mItems.add(item1002);
        mItems.add(item1003);
        mItems.add(item1004);
        mItems.add(item1005);
        mItems.add(item1006);
        mItems.add(item1007);

        mTvReset.setOnClickListener(this);

        mTvTitle.setText("基本图像处理");
        ToolbarHelper.getInstance().initToolbar(this,mToolbar,R.mipmap.ic_back);

        //需要注意从imageview或者 decodeResource获取的bitmap是不可以再进行像素调整的，类似setPixels方法是不可以再使用的，无效
        //createBitmap生成的bitmap后期还可以继续修改
        //可通过bitmap.isMutable()判断是否有效
        //不要手动recycle()ImageView中的bitmap资源，它是由系统内部来控制的，若手动释放，会造成下次再使用时候，报错等异常问题
        /*BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        mSrcBitmap = BitmapFactory.decodeResource(getResources(),R.mipmap.lena,options);
        mSrcBitmapWidth = options.outWidth;
        mSrcBitmapHeight = options.outHeight;*/

        //原始图，永远不处理
        mSrcBitmap = ((BitmapDrawable)(mIvSrc.getDrawable())).getBitmap();
        mSrcBitmapWidth = mSrcBitmap.getWidth();
        mSrcBitmapHeight = mSrcBitmap.getHeight();

        //缓冲图
        mTmpBitmap = Bitmap.createBitmap(mSrcBitmapWidth,mSrcBitmapHeight, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mTmpBitmap);
        mPaint = new Paint();
        mCanvas.drawBitmap(mSrcBitmap, 0, 0, mPaint);

        //recyclerview初始化
        mAdapterImageProgress = new AdapterImageProgress(this, mItems,mSrcBitmap,mTmpBitmap,mIvSrc);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRvImgProgress.setLayoutManager(linearLayoutManager);
        mRvImgProgress.setAdapter(mAdapterImageProgress);
        mRvImgProgress.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }

    @Override
    protected void myOnClick(View view) {
        super.myOnClick(view);
        switch(view.getId()){
            case R.id.tv_reset:
                mCanvas.drawBitmap(mSrcBitmap, 0, 0, mPaint); //重置
                mIvSrc.setImageBitmap(mSrcBitmap);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mTmpBitmap!=null)
            mTmpBitmap.recycle();
        mTmpBitmap=null;
    }
}