package com.wj.demo.ui.activity;

import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wj.demo.R;
import com.wj.demo.app.Config;
import com.wj.demo.helper.CameraSurfaceHelper;
import com.wj.demo.ui.base.BaseActivity;
import com.wj.demo.weiget.surfaceview.CameraSurfaceView;
import com.wj.library.helper.ToastHelper;
import com.wj.library.helper.ToolbarHelper;
import com.wj.library.widget.GifView;

import butterknife.BindView;

/**
 * 摄像头单帧图像处理
 */
public class DemoCameraImageProcessActivity extends BaseActivity {
    private static String TAG = DemoCameraImageProcessActivity.class.getSimpleName();

    @BindView(R.id.camera_sv)
    CameraSurfaceView mSvCamera;

    @BindView(R.id.bt_take_pic)
    Button mBtTakePic;

    @BindView(R.id.bt_reset)
    Button mBtReset;

    private CameraSurfaceHelper mCameraSurfaceHelper;
    private SurfaceHolder       mSurfaceHolder;

    @Override
    protected int getLayout() {
        return R.layout.activity_base_camera_process;
    }

    @Override
    protected void initViewAndData() {
        mBtTakePic.setOnClickListener(this);
        mBtReset.setOnClickListener(this);

        mSvCamera.setFTPS(Config.FPS);
        mCameraSurfaceHelper = CameraSurfaceHelper.getInstance(mSvCamera, 0);
        mCameraSurfaceHelper.setOnPreviewListener(new CameraSurfaceHelper.OnPreviewFrame() {
            @Override
            public void previewFrame(byte[] data, float widthScale, float heightScale) {

            }
        });

        mCameraSurfaceHelper.setOnCameraListener(new CameraSurfaceHelper.OnCameraListener() {
            @Override
            public void takePictureOver(String path) {


            }
        });

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mCameraSurfaceHelper.autoFocus();
        return super.onTouchEvent(event);
    }

    @Override
    protected void myOnClick(View view) {
        super.myOnClick(view);
        switch (view.getId()){
            case R.id.bt_take_pic:
                mCameraSurfaceHelper.takePicture();
                break;
            case R.id.bt_reset:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mCameraSurfaceHelper!=null)
            mCameraSurfaceHelper.release();
    }
}
