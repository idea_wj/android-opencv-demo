package com.wj.library.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wj.library.R;


public final class ToastUtil {


    public static final String TAG = ToastUtil.class.getSimpleName();
    private static int CONTENT_LENGTH_FOR_LONG_DURATION_TOAST = 15;//toast长度超过这个是使用long

    private static Context sContext;

    private static Resources sResources = null;

    private static Toast sToast = null;

    private static Handler sHandler = new Handler(Looper.getMainLooper());

    public static void init(Context context) {
        sContext = context;
        sResources = sContext.getResources();
    }

    public static void postShow(int textResId) {
        postShow(sResources.getText(textResId));
    }

    public static void postShow(int textResId, int textSize) {
        postShow(sResources.getText(textResId), textSize);
    }

    public static void postShow(CharSequence text) {
        postShow(text, 18, R.drawable.toast_util_bg);
    }

    public static void postShow(CharSequence text, int textSize) {
        postShow(text, textSize, R.drawable.toast_util_bg);
    }

    /**
     * @param text           要显示的文案
     * @param textSize       字体大小
     * @param backResourceId 背景图
     */
    public static void postShow(final CharSequence text, final float textSize, final @DrawableRes int backResourceId) {
        if (sToast != null) {
            sToast.cancel();
        }

        if (TextUtils.isEmpty(text)) {
            return;
        }

        sHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    final TextView messageTv = (TextView) LayoutInflater.from(sContext).inflate(R.layout.layout_toast, null, false);
                    messageTv.setText(text);
                    messageTv.setTextSize(textSize);
                    setBackgroundResource(messageTv, backResourceId);
                    sToast = new Toast(sContext);
                    sToast.setView(messageTv);
                    sToast.setGravity(Gravity.CENTER_VERTICAL, sToast.getXOffset(), sToast.getYOffset());
                    sToast.setDuration(text.length() < CONTENT_LENGTH_FOR_LONG_DURATION_TOAST ?
                            Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
                    sToast.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private static void setBackgroundResource(@NonNull View view, @DrawableRes int resId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            int paddingTop = view.getPaddingTop();
            int paddingLeft = view.getPaddingLeft();
            int paddingRight = view.getPaddingRight();
            int paddingBottom = view.getPaddingBottom();
            view.setBackgroundResource(resId);
            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        } else {
            view.setBackgroundResource(resId);
        }
    }

    /**
     * 短，资源文件
     * @param msg
     */
    public static void showShort(int msg) {
        Toast.makeText(sContext, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 短，自定义字符串
     * @param msg
     */
    public static void showShort(String msg){
        Toast.makeText(sContext, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 长，资源文件
     * @param msg
     */
    public static void showLong(int msg) {
        Toast.makeText(sContext, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * 长，自定义字符串
     * @param msg
     */
    public static void showLong(String msg){
        Toast.makeText(sContext, msg, Toast.LENGTH_LONG).show();
    }
}
