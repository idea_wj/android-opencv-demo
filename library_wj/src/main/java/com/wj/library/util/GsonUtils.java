package com.wj.library.util;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @version 1.0
 * gson的jar包提供的方法统一在这进行处理，处理到结果后，再统一返回
 * @author idea_wj 2015-08-04
 */
public class GsonUtils {
    private static String TAG = GsonUtils.class.getSimpleName();
    
    public static String carToStr(InputStream inputStream) throws IOException {
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        // 数组长度
        byte[] buffer = new byte[1024];
        // 初始长度
        int len = 0;
        // 循环
        while ((len = inputStream.read(buffer)) != -1) {
            arrayOutputStream.write(buffer, 0, len);
//            return arrayOutputStream.toString();
        }
        return arrayOutputStream.toString();
    }

    public static <T> String getListJson(ArrayList<T> list){
        Gson gson = new Gson();
        return gson.toJson(list);
    }

    /**
     * 通过Json串获取单一实例
     *
     * @param jsonString
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T getObject(String jsonString, Class<T> cls) {
        T t = null;
        try {
            Gson gson = new Gson();
            t = gson.fromJson(jsonString, cls);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 获取一个实例集
     *
     * @param jsonString
     * @param <T>
     * @return
     */
    public static <T> ArrayList<T> getObjects(String jsonString, Type type) {
    	ArrayList<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            list = gson.fromJson(jsonString, type);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 返回string类型的集合
     *
     * @param jsonString
     * @return
     */
    public static List<String> getStringList(String jsonString) {
        List<String> list = new ArrayList<String>();
        try {
            Gson gson = new Gson();
            list = gson.fromJson(jsonString, new TypeToken<List<String>>() {
            }.getType());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

    /**
     * 返回long类型的集合
     *
     * @param jsonString
     * @return
     */
    public static <T> List<T> getList(String jsonString) {
        List<T> list = new ArrayList<T>();
        try {
            Gson gson = new Gson();
            list = gson.fromJson(jsonString, new TypeToken<List<T>>() {
            }.getType());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }



    /**
     * list map型
     *
     * @param jsonString
     * @return
     */
    public static List<Map<String, Object>> listKeyMap(String jsonString) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        try {
            Gson gson = new Gson();
            list = gson.fromJson(jsonString, new TypeToken<List<Map<String, Object>>>() {
            }.getType());
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

    /**
     * 将一个实体类转换为json字符串
     * @param object
     * @return
     */
    public static String getObject2Json(Object object){
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    /**
     * 将一个指定字段的实体类转换为json字符串
     * @param object
     * @return
     */
    public static String getExposeObject2Json(Object object){
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(object);
    }

    public static JsonElement getObject2JsonElement(Object object){
        Gson gson = new Gson();
        return gson.toJsonTree(object);
    }
}