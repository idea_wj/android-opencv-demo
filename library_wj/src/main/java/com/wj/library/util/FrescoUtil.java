package com.wj.library.util;

import android.app.ActivityManager;
import android.content.Context;
import android.net.Uri;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.facebook.imagepipeline.core.ImagePipelineFactory;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.wj.library.widget.fresco.DefaultBitmapMemoryCacheParamsSupplier;


/**
 * Created by sangular-wj on 2017/11/27.
 */

public class FrescoUtil {

    /**
     * 使用自定义缓存配置文件初始化
     * @param context
     */
    public static void initByConfig(Context context){
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        DefaultBitmapMemoryCacheParamsSupplier supplier = new DefaultBitmapMemoryCacheParamsSupplier(activityManager);
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(context)
                .setBitmapMemoryCacheParamsSupplier(supplier)
                .build();
        Fresco.initialize(context,config);
    }

    /**
     * 使用默认值初始化
     * @param context
     */
    public static void initDefault(Context context){
        Fresco.initialize(context);
    }

    /**
     * 统一管理图片显示，避免不同位置添加file://造成混乱
     * @param width
     * @param height
     * @param path
     * @param view
     * @return
     */
    public static PipelineDraweeController getImageController(int width, int height, String path, SimpleDraweeView view,boolean isAutoPlay){
        if(StringUtil.isEmpty(path))
            return null;
        ImageRequest request = null;
        if(path.startsWith("http")){
            request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(path))
                    .setResizeOptions(new ResizeOptions(width, height))
                    .build();
        }else {
            request = ImageRequestBuilder.newBuilderWithSource(Uri.parse("file://" + path))
                    .setResizeOptions(new ResizeOptions(width, height))
                    .build();
        }
        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setOldController(view.getController())
                .setImageRequest(request)
                .setAutoPlayAnimations(isAutoPlay)
                .build();
        return controller;
    }

    /**
     * 统一管理图片显示，避免不同位置添加file://造成混乱
     * @param path
     * @return
     */
    public static PipelineDraweeController getImageController(String path,boolean isAutoPlay){

        PipelineDraweeController controller = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(path))
                .setAutoPlayAnimations(isAutoPlay)
                .build();
        return controller;
    }

    /**
     * SimpleDraweeView的url直接显示都通过此实现
     * @param path
     */
    public static void sdvShow(String path,SimpleDraweeView sdv){
        if(!StringUtil.isEmpty(path)) {
            if (path.startsWith("http")||path.startsWith("http"))
                sdv.setImageURI(Uri.parse(path));
            else
                sdv.setImageURI(Uri.parse("file://" + path));
        }else {
            sdv.setImageURI(Uri.parse("nothing")); //不显示任何内容
        }
    }

    public static void sdvShow(int res,SimpleDraweeView sdv){
        sdv.setImageURI(Uri.parse("res:///"+res)); //不显示任何内容
    }

    public static void clearAllMemoryCaches(){
        ImagePipelineFactory.getInstance().getImagePipeline().clearMemoryCaches();
    }

}
