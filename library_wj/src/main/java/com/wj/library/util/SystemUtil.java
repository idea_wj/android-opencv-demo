package com.wj.library.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

/**
 * Created on 2017/9/25.
 *
 * @author xinxiong
 *         <p>
 *         系统的一些常用工具类
 */

public class SystemUtil {

    /**
     * 比较当前版本号是否大于指定版本号
     *
     * @param specifyVersionCode 指定的版本号
     * @return if greater return true
     */
    public static boolean compareVersionCode(int specifyVersionCode) {

        return Build.VERSION.SDK_INT >= specifyVersionCode;
    }


    /**
     * 获取跳转到权限设置页面
     *
     * @param context 上下文
     * @return intent
     */
    public static Intent getPermissionSettingIntent(Context context) {
        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (compareVersionCode(Build.VERSION_CODES.GINGERBREAD)) {
            intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            intent.setData(Uri.fromParts("package", context.getPackageName(), null));
        } else if (!compareVersionCode(Build.VERSION_CODES.GINGERBREAD)) {
            intent.setAction(Intent.ACTION_VIEW);
            intent.setClassName("com.android.settings", "com.android.settings.InstalledAppDetails");
            intent.putExtra("com.android.settings.ApplicationPkgName", context.getPackageName());
        }
        return intent;
    }
}
