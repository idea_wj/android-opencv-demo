package com.wj.library.net.okhttp.callback;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author sangular-wj
 * 关于callback重写：
 * 1. 建议使用抽象类
 * 2. onStart()为执行请求前的初始化方法，若不需要可先在当前callback类中实现空方法
 * 3. onFailure()错误异常相关问题的处理，父类已经实现，若无必要实现，也可在当前类中先实现
 * 4. onFinish()无论成功还是失败，最后都会调用的方法，若有需要可手动重写实现
 * 5. 如此一来，调用此callback的地方仅仅需要重写onFinish以及onSuccess方法
 *
 */

public abstract class StringCallback extends BaseCallback<String>{

    @Override
    public void onStart(Request request) {}

    @Override
    public void onFailure(Call call, Response response, Exception exception) {}

    @Override
    public void onFinish() {

    }

    @Override
    public String parseNetworkResponse(Response response) {
        try {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}