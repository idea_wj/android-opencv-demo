package com.wj.library.net;

/**
 * Created by sangular-wj on 2017/11/17.
 */

public class HTTPCode {
    /**
     * 200 调用成功，运行结果参考API说明
     */
    public static final String SUCCESS = "200";

    /**
     * 98 发送失败
     */
    public static final String ERROR = "98";

    /**
     * 300 调用成功，业务错误
     */
    public static final String FAIL_BUSINESS_ERROR = "300";

    /**
     * 400 无效数据
     */
    public static final String FAIL_INVALID_DATA = "400";

    /**
     * 401 用户认证失败
     */
    public static final String FAIL_INVALID_USER = "401";

    /**
     * 403 用户无权访问
     */
    public static final String FAIL_INVALID_AUTH = "403";

    /**
     * 404 无效URI
     */
    public static final String FAIL_INVALID_URI = "404";

    /**
     * 500 服务端错误
     */

    public static final String FAIL_SERVER_ERROR = "500";
}
