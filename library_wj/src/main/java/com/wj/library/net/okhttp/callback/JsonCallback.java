package com.wj.library.net.okhttp.callback;

import android.util.Log;

import com.wj.library.net.okhttp.Response.BaseResponse;
import com.wj.library.util.GsonUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;

/**
 *
 * @author sangular-wj
 * 关于callback重写：
 * 1. 建议使用抽象类
 * 2. onStart()为执行请求前的初始化方法，若不需要可先在当前callback类中实现空方法
 * 3. onFailure()错误异常相关问题的处理，父类已经实现，若无必要实现，也可在当前类中先实现
 * 4. 如此一来，调用此callback的地方仅仅需要重写onFinish以及onSuccess方法
 * @param <C>
 */
public class JsonCallback<C extends BaseResponse> extends BaseCallback<C> {
    private static final String TAG = JsonCallback.class.getSimpleName();

    private Class<C> classType;


    public JsonCallback(Class<C> classType) {
        this.classType = classType;
    }

    @Override
    public void onStart(Request request) {

    }

    @Override
    public void onFailure(Call call, Response response, Exception exception) {
        call.cancel();
        Log.e(TAG,"~~~~~~~~~~~~~~~失败");
    }

    @Override
    public C parseNetworkResponse(Response response) {
        C gsonResponse = null;
        if (response.body() != null && response.body().charStream() != null) {
            String s = null;
            try {
                s = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            gsonResponse = GsonUtils.getObject(s, classType);
        }
        return gsonResponse;
    }

    @Override
    public void onSuccess(C response) {
        Log.e(TAG,"~~~~~~~~~~~~~~~成功");
    }

    @Override
    public void onFinish() {


    }
}
