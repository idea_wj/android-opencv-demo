package com.wj.library.net.okhttp.Response;

/**
 * Created on 2017/9/29.
 *
 * @author wj
 */
public class BaseResponse<T> {
    private String status;
    private String message;
    private T data;
    private T page;

    public T getPage() {
        return page;
    }

    public void setPage(T page) {
        this.page = page;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}

