package com.wj.library.helper;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.wj.library.base.MyBaseAppManager;


/**
 * @author idea_wj 2015-08-04
 * @version 1.0
 *          UI操作的通用管理
 */
public class UIHelper {

    private static UIHelper instance=null;

    public static UIHelper getInstance(){
        if(instance==null){
            synchronized(UIHelper.class){
                if(null==instance){
                    instance=new UIHelper();
                }
            }
        }
        return instance;
    }

    public void activityFinish(AppCompatActivity activity) {
        MyBaseAppManager.getInstance().finishActivity(activity);
    }

    /**
     * Activity页面之间的简单跳转,跳转后，前一个activity不被finish
     */
    public void startActivityNoFinish(AppCompatActivity activity, Intent intent) {
        activity.startActivity(intent);
        MyBaseAppManager.getInstance().addActivity(activity);
    }


    /**
     * Activity页面之间跳转并返回结果
     * 不适用于activity
     */
    public void startActivityForResult(AppCompatActivity activity, Intent intent, int RequestCode) {
        activity.startActivityForResult(intent, RequestCode);
        MyBaseAppManager.getInstance().addActivity(activity);
    }

    /**
     * 从fragment切换到下一个Activity页面之间跳转并返回结果
     */
    public static void startFragmentActivityForResult(Fragment fragment, AppCompatActivity activity, Intent intent, int RequestCode) {
        fragment.startActivityForResult(intent, RequestCode);
        MyBaseAppManager.getInstance().addActivity(activity);
    }

    /**
     * Activity页面之间的简单跳转,跳转后，前一个activity被finish
     */
    public void startActivityAndFinish(AppCompatActivity activity, Intent intent) {
        activity.startActivity(intent);
        MyBaseAppManager.getInstance().finishActivity(activity);  //栈顶先除去
    }

    /**
     * Activity页面之间的简单跳转
     */
    public void startActivity(AppCompatActivity activity, Intent intent) {
        activity.startActivity(intent);
    }
}
